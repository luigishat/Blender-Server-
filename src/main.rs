use serde::{Serialize, Deserialize};
use std::io::{Result, ErrorKind};
use tokio::net::{TcpStream, TcpListener};
use std::sync::{Arc, Mutex};
use tokio::sync::Mutex as tokioex;
use tokio::io::{AsyncReadExt, AsyncWriteExt};


#[derive(Serialize, Deserialize, Debug)]
struct Packet {
    id: Role,
    start_frame: u32,
    end_frame: u32,
    file: Vec<u8>,
    file_name: String,
    file_size: u64
}
#[derive(Serialize, Deserialize, Debug)]
enum Role{
    Slave,
    Customer,
    Messenger
}

#[derive(Serialize, Deserialize, Debug)]
struct Job {
    file: Vec<u8>,
    frame_range: (u32, u32)
}



#[tokio::main]
async fn main() -> Result<()> {
    let ears = TcpListener::bind("localhost:27182").await.expect("Couldn't bind to local socket");
    let slaves:Arc<Mutex<Vec<TcpStream>>> = Arc::new(Mutex::new(Vec::new()));
    let master:Arc<tokioex<Option<TcpStream>>> = Arc::new(tokioex::new(None));
    let file:Arc<Mutex<Option<Packet>>> = Arc::new(Mutex::new(None));
    let count_base: Arc<Mutex<usize>> = Arc::new(Mutex::new(0));
    let mut stop = false;
    const BARRIER:usize = 1;
    loop {

        let (socket, _) = ears.accept().await?;
        let slavess = slaves.clone();
        let leader = master.clone();
        let fake = file.clone();
        let count = count_base.clone();
        tokio::spawn(async move {
            if let Some(i) = handle_packet(socket, slavess, leader, count).await{
                let mut fake = fake.lock().unwrap();
                *fake = Some(i);
            }
        }).await.unwrap();
        let slaves = slaves.clone();
        let file = file.clone();
        let file_unlocked = file.lock().unwrap();
        let count = count_base.clone();

        if master.try_lock().unwrap().is_none() {
            stop = false;
        }

        if  slaves.lock().unwrap().len() >= BARRIER && master.try_lock().unwrap().is_some() && !stop {
            let frame_list = divide_frames(&file_unlocked.as_ref().unwrap().start_frame,
            &file_unlocked.as_ref().unwrap().end_frame,
            slaves.lock().unwrap().len() as u32);

            println!("Above send task");
            send_task(&mut *slaves.lock().unwrap(), & *file_unlocked, frame_list).await;
            stop = true;
        }
    }
    
}

#[derive(Deserialize, Serialize, Debug)]
struct RenderdPng {
    file: Vec<u8>,
    file_name: String,
}


async fn handle_packet(mut stream: TcpStream, clients: Arc<Mutex<Vec<TcpStream>>>,
                 master: Arc<tokioex<Option<TcpStream>>>, count: Arc<Mutex<usize>>)  -> Option<Packet> {
    stream.readable().await.unwrap();
    let mut buff: [u8; 8] = [0;8];
    let mut new_buff: Vec<u8>;

    println!("Somebody connected");

    loop {
        match stream.read_exact(&mut buff).await {
            Ok(r) => {
                if r > 0{
                    println!("{}", r);
                }

                //let end = buff.iter().position(|x| *x == 47 as u8).unwrap();
                //let ulen = &buff[0..end];
                
                match bincode::deserialize(&buff) {
                    Ok(transmission) => {
                        let transmission:usize = transmission;
                        println!("Size of transmission is {}", &transmission);
                        new_buff = vec![0; transmission];
                        break;
                        //for i in end..buff.len() {
                        //    new_buff.push(buff[i])
                        //}

                    }
                    Err(_e) => {
                        println!("Error buffer = {:?}", &buff);
                        panic!();
                    }
                }
            }
            Err(e) => {
                println!("Couldnt Read because of {}", e);
                panic!();
            }
        }
    }
    println!("we broke out");

        match stream.read_exact(&mut new_buff[..]).await {
            Ok(r) => {
                println!("Second Read loop");
                if r > 0{
                    println!("{}", r);
                }
                match bincode::deserialize(&new_buff){
                    Ok(transmission) => {
                        let transmission:Packet = transmission;
                        if let Role::Customer = transmission.id{

                            if transmission.file_name == String::from("Done") {
                                let mut master = master.lock().await;
                                *master = None;
                                return None;
                            }
                            let mut master = master.lock().await;
                            *master = Some(stream);
                            return Some(transmission);
                        }
                    
                        if let Role::Messenger = transmission.id {
                            if transmission.file_name == "Done" {
                                let mut count = count.lock().unwrap();
                                *count -= 1;
                                let mut slaves = clients.lock().unwrap();
                                slaves.drain(0..).collect::<Vec<TcpStream>>();
                                return None;
                            }
                            return_to_sender(transmission, stream, clients, count, master).await;
                            return None;
                        }

                    panic!("This should not happen");
                    }

                    Err(_e) => {
                        match bincode::deserialize(&new_buff) {
                            Ok(transmission) => {
                                //println!("Slave time !");
                                if let Role::Slave = transmission {
                                    let mut count = count.lock().unwrap();
                                    *count += 1;
                                    clients.lock().unwrap().push(stream);
                                    return None;
                                }
                                panic!();
                            }
                            Err(_) => {
                                //continue;
                                panic!();
                                }
                            }
                        }
                }
            }

            Err(ref e) if e.kind() == ErrorKind::WouldBlock => {
                println!("We aint reading");
                panic!();
                //continue;
            }
            
            Err(e) => {
                println! ("Error: {}", e);
                panic!("Some Error occured");
            }
        }
    //}
}


async fn return_to_sender(transmission: Packet, stream: TcpStream, 
        clients: Arc<Mutex<Vec<TcpStream>>>,count: Arc<Mutex<usize>>,
        master: Arc<tokioex<Option<TcpStream>>>){
    if transmission.file_name == String::from("Done") {
        let mut clients = clients.lock().unwrap();
        let clients: &mut Vec<TcpStream> = clients.as_mut();
        let mut iterator = clients.iter();
        let iterator = iterator.position(|x| x.local_addr().unwrap()
                                     == stream.local_addr().unwrap());
        clients.remove(iterator.unwrap());
        let mut count = count.lock().unwrap();
        *count = *count - 1;
    }


    send_stuff(&transmission, master.lock().await.as_mut().unwrap()).await;
}

async fn send_task(clients: &mut Vec<TcpStream>, file: &Option<Packet>,
    frame_range: Vec<(u32, u32)>) {
    let file = file.as_ref().unwrap();
    let mut count = 0;
    for mut i in clients.iter_mut() {
        let frame_range = frame_range.clone();
            let uhh = Job {
                file: file.file.to_vec(),
                frame_range: frame_range[count.clone()]
            };
        send_stuff(&uhh, &mut i).await;
        count += 1;
    }
}

fn divide_frames(start_frame: &u32, end_frame: &u32, num_slaves: u32) -> Vec<(u32, u32)> {
    let size = end_frame - start_frame;
    let range = size / num_slaves ;
    let mut frame_list = Vec::new();
    //println!("{}", num_slaves);
    for i in 0..num_slaves {
        if i == 0 {
            frame_list.push((*start_frame, start_frame + range));
            continue;
        }

        let new_starting_frame = start_frame + range * i;
        let new_end_frame = new_starting_frame + range;
        if i + 1 == num_slaves {
            frame_list.push((new_starting_frame, *end_frame));
            break;
        }
        frame_list.push((new_starting_frame, new_end_frame - 1));
    }
    println!("{:?}", frame_list);
    return frame_list;
}

async fn send_stuff<T>(buff: &T, stream: &mut TcpStream)
where T: serde::Serialize {
    let file = bincode::serialize(&buff).unwrap();
    let size = file.len();

    println!("Sending a {} sized transmission", &size);

    stream.write_all(&bincode::serialize(&size).unwrap()).await.unwrap();
    stream.write_all(&file).await.unwrap();
}
